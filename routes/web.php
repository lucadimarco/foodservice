<?php
use App\Food\Product;
use Illuminate\Support\Facades\Route;
use App\User_Product;
use App\User;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/product', 'ProductController@index')->name('product.home');
    Route::get('/order', 'OrderController@index')->name('order.home');
    Route::get('/carrello', 'CarrelloController@index')->name('carrello.home');
    Route::post('/addcart/{id}', 'ProductController@addcart')->name('addcart');
    Route::delete('/removecart/{ass_id}', 'ProductController@removecart')->name('removecart');
});


/*-----
Route::get('/provaprod',function(){
    return Product::all();
    
});
Route::get('/join',function(){
    $user = User::find(Auth::id());
    return User_Product::all()->where('users_id', $user->id);
});
Route::get('/alljoin',function(){
    return User_Product::all();
});--/*
