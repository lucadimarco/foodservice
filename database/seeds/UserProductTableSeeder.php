<?php

use Illuminate\Database\Seeder;
use App\User_Product;

class UserProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User_Product::class, 10)->create();
    }
}
