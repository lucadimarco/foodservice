<?php

use Illuminate\Database\Seeder;
use App\Order_Product;

class OrderProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Order_Product::class, 10)->create();
    }
}
