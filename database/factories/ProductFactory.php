<?php

use Faker\Generator as Faker;
use App\User;
use App\Food\Product;
use App\User_Product;
use App\Order;
use App\Order_Product;

/**
 * factory per testare la tabella dei prodotti
 */
$factory->define(App\Food\Product::class, function (Faker $faker) {
    return [
        'product_name' => $faker->name,
        'taxable_price' => $faker->randomFloat(2, 10, 90),
        'rate' => random_int(4, 22)
    ];
});

$factory->define(App\User_Product::class, function (Faker $faker) {
    return [
        'users_id' => User::inRandomOrder()->first()->id,
        'product_id' => Product::inRandomOrder()->first()->id,
    ];
});

$factory->define(App\Order_Product::class, function (Faker $faker) {
    return [
        'order_id' => Order::inRandomOrder()->first()->id,
        'product_id' => Product::inRandomOrder()->first()->id,
    ];
});
