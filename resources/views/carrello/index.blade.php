@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row mt-5">
        <div class="col-md-4">
                    <div class="card border-primary mb-3" style="max-width: 20rem;">
                            <div class="card-header">Completa l'ordine</div>
                            <div class="card-body">
                            <h4 class="card-title">Il Mio Carrello</h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a class=" btn btn-success" href="#" data-toggle="modal" data-target="#conferma_ordine">Completa l'Ordine</a>
                            </div>
                            <div class="modal fade " aria-labelledby="exampleModalLabel" id="conferma_ordine" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" >
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title">Conferma Ordine</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                            <h6>indirizzo</h6>
                                                <input type="text" name ="indirizzo" placeholder="Inserisci l'indirizzo" value="" class="form-control btn-rounded">
                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-success">Conferma Ordine</button>
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                        </div>
                        <div class="alert-wrapper"></div>
                        
        </div>
        <div class="col-md-8">
                <table class="table table-hover">
                        <thead>
                        <tr class="table-primary">
                            <th scope="col">Nome</th>
                            <th scope="col">Prezzo</th>
                            <th scope="col">Voto</th>
                            <th scope="col">Azioni</th>
                        </tr>
                        </thead>
                        <tbody>
                        <form>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        @foreach ($carts as $cart)
                            <tr class="product_row{{ $cart->ass_id }}">
                                <td>{{ $cart->product_name }}</td>
                                <td>{{ $cart->taxable_price }}€</td>
                                <td>{{ $cart->rate }}</td>
                                <td><a href="/removecart/{{$cart->ass_id}}" id="{{$cart->ass_id}}" class="btn btn-danger">Rimuovi</a></td>
                            </tr>
                        @endforeach
                        <form>
                        </tbody>
                </table>
        </div>
    </div>
    
</div>

@section('script')
            @parent
      <script>
      $(document).ready(function() {

        $('.alert-wrapper > div').fadeOut(5000);

    $('td').on('click','.btn-danger',function(ele){
        var idrow = $(this).attr('id');
        
        console.log(ele);

        ele.preventDefault();
           var urlOfferta= $(this).attr('href');
           console.log(urlOfferta);
           

            $.ajax(urlOfferta,
           {
               method:'DELETE',
               data:{
                   '_token' : $('#_token').val()
               },
               complete : function(resp){
                   console.log(resp);
                   if(resp.responseText == 1 ){
                       console.log("food");
                       console.log(idrow);
                        $(".product_row"+idrow).remove();
                        var successHtml = '<div class="alert alert-dismissible alert-secondary"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Prodotto eliminato con successo</strong></div>';
    console.log(successHtml);
                            $('.alert-wrapper').append(successHtml);
                            $('.alert-wrapper').find('.alert').delay(3000).fadeOut('slow');

                   }else{
                       alert('problema con il server');
                   }

               }
           }
           )

          
    });
      });

      
      </script>
      
      @endsection


@endsection