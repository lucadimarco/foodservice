@extends('layouts.app')

@section('content')


 
<form>
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <div class="page-product container-fluid mt-5">
      <h3 class="my-4">
          I Nostri Prodotti
        </h3>
        <div class="alert-wrapper product-alert-wrapper "></div>
        <table class="table table-hover">
                <thead>
                  <tr class="table-primary">
                    <th scope="col" class="text-center">Nr</th>
                    <th scope="col" class="text-center">Nome</th>
                    <th scope="col" class="text-center">Prezzo</th>
                    <th scope="col" class="text-center">Voto</th>
                    <th scope="col" class="text-center">Azioni</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                  <tr class="product_row{{ $product->id }}" >
                    <td class="text-center"><input type="hidden" name="product_id" id="prod" value="{{ $product->id }}">{{ $product->id }}</td>
                    <td class="text-center">{{ $product->product_name }}</td>
                    <td class="text-center">{{ $product->taxable_price }}</td>
                    <td class="text-center">{{ $product->rate }}</td>
                    <td scope="row" class="text-center"><a id="{{$product->id}}" href="/addcart/{{$product->id}}" class="btn btn-outline-success">Aggiungi al carrello</a></td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
              <div class="pullright">
              {{ $products->render("pagination::bootstrap-4") }}
              </div>
  </div>
</form>
@section('script')
            @parent
      <script>
      $(document).ready(function() {


    $('td').on('click','.btn-outline-success',function(ele){
        var idrow = $(this).attr('id');
        console.log(idrow);
        console.log(ele);

        ele.preventDefault();
           var urlOfferta= $(this).attr('href');
           console.log(urlOfferta);
           

            $.ajax(urlOfferta,
           {
               method:'POST',
               data:{
                   '_token' : $('#_token').val()
               },
               complete : function(resp){
                   console.log(resp);
                   if(resp.responseText == 1 ){
                   
                        

                   }else{
                       
                       var successHtml = '<div class="alert alert-dismissible alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Prodotto Aggiunto con successo</strong></div>';
                            $('.product-alert-wrapper').append(successHtml);
                            
                            $('.product-alert-wrapper').find('.alert').delay(3000).fadeOut('slow');
                   }

               }
           }
           )

          
    });
      });

      
      </script>
      
      @endsection

@endsection