@extends('layouts.app')

@section('content')

<form>
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <div class="page-product container-fluid mt-5">
      <h3 class="my-4">
          I Nostri Ordini
        </h3>
        <div class="alert-wrapper product-alert-wrapper "></div>
        <table class="table table-hover">
                <thead>
                  <tr class="table-primary">
                    <th scope="col" class="text-center">Nr</th>
                    <th scope="col" class="text-center">Nome</th>
                    <th scope="col" class="text-center">Prezzo</th>
                    <th scope="col" class="text-center">Voto</th>
                    <th scope="col" class="text-center">Azioni</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($orders as $product)
                  <tr class="product_row{{ $product->id }}" >
                    <td class="text-center"><input type="hidden" name="product_id" id="prod" value="{{ $product->id }}">{{ $product->id }}</td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td scope="row" class="text-center"><a id="" href="/addcart/" class="btn btn-outline-success">Aggiungi al carrello</a></td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
              <div class="pullright">
              
              </div>
  </div>
</form>

@endsection