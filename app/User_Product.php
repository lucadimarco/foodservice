<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Food\Product;

class User_Product extends Model
{
    protected $table = 'user__products';
    public function user() {
        return $this->belongsTo(User::class);
      }
      public function product() {
          return $this->belongsTo(Product::class);
      }
      public function addProduct($user_product) {
        $this->create($user_product);
      }
}
