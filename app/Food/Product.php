<?php

namespace App\Food;

use App\User_Product;
use App\Order_Product;

/**
 * Class Product
 * @package App\Food
 *
 * modello per la tabella dei prodotti
 */
class Product extends Base
{
    public function user_product() {

        return $this->belongsTo(User_Product::class);
      }

      public function order_product() {

        return $this->belongsTo(Order_Product::class);
      }
}
