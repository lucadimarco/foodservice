<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Food\Product;

class Order_Product extends Model
{
    protected $table = 'order__products';
    public function order() {
        return $this->belongsTo(Order::class);
      }
  
      public function product() {
          return $this->belongsTo(Product::class);
      }
}
