<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Food\Product;
use App\User;
use App\User_Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(){
        $products = Product::paginate(7);
        return view('product.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function addcart($id){
        $user = User::find(Auth::id());
        $product = Product::find($id);
        $user_product = new User_Product();
        $user_product->product_id = $product->id;
        $user_product->users_id = $user->id;
        $res = $user_product->save();
    }
    public function removecart($id){
        $sql="delete from user__products where ass_id = :id";
        return DB::delete($sql,['id'=>$id]);
    }
}
