<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Food\Product;
use App\User_Product;
use App\User;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class CarrelloController extends Controller
{
    public function index()
    {
        $user = User::find(Auth::id());
        $carts = DB::table('user__products')
    ->join('products', 'products.id', '=', 'user__products.product_id')
    ->join('users', 'users.id', '=', 'user__products.users_id')
    ->where('users_id', $user->id)
    ->get();
        return view('carrello.index',compact('carts'));
    }


    
}
