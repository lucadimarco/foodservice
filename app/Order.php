<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table= "order";
    public function order_product() {

        return $this->hasmany(Order_Product::class);
      }
}
